﻿using UnityEngine;
using System.Collections;

public class FixRotation : MonoBehaviour {

	Quaternion rotation;
	
	void Awake()
	{
		rotation = transform.rotation;
	}

	void LateUpdate()
	{
		transform.rotation = rotation;
		transform.position = new Vector3(transform.position.x, transform.parent.transform.position.y + 1.0f, 0.0f);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
