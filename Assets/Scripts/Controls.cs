﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Controls : MonoBehaviour {

	bool jumping;
	public GameObject bonus;

	// Use this for initialization
	void Start () {
		jumping = false;
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.Escape)){
			GameplayManager.instance.pause();
		}

		if (jumping) {
			if ((Input.GetKey (KeyCode.LeftArrow)) || (Input.GetKey (KeyCode.A))){;
				GetComponent<Rigidbody2D>().AddForce(new Vector2(-15.0f, 0.0f));
			}
			if ((Input.GetKey (KeyCode.RightArrow)) || (Input.GetKey (KeyCode.D))){;
				GetComponent<Rigidbody2D>().AddForce(new Vector2(15.0f, 0.0f));
			}
		}
	}

	void FixedUpdate() {

		Vector2 v = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, GetComponent<Rigidbody2D> ().velocity.y);
		
		if ((int)v.y > 65){
			GetComponent<Rigidbody2D>().drag = 5.55f;
		}else{
			GetComponent<Rigidbody2D>().drag = 0.01f;
		}

		float a = GetComponent<Rigidbody2D> ().angularVelocity;

		if(Mathf.Abs((int)a) > 650) {
			GetComponent<Rigidbody2D>().angularDrag = 8.3f;
		}else{
			GetComponent<Rigidbody2D>().angularDrag = 0.01f;
		}

	}

	void OnCollisionEnter2D(Collision2D coll) {

		if (coll.gameObject.tag == "Character") {
			coll.gameObject.GetComponent<Character>().setColliders(false);
			coll.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0.0f;
			coll.gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f,1.0f,1.0f,0.3f);
			Vector3 pos = new Vector3(coll.transform.position.x, coll.transform.position.y + 2.0f, coll.transform.position.z);
			GameObject b = Instantiate(bonus, pos, coll.transform.rotation) as GameObject;
			coll.gameObject.GetComponent<Character>().vanish();
			b.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0.0f, 2000.0f));
			b.GetComponentInChildren<Explosion>().detonate();
		}

		if (coll.gameObject.tag == "Bonus") {
			GameplayManager.instance.bonus(coll.gameObject);
		}

		if(coll.gameObject.tag == "Ground"){
			jumping = false;
		}
	}

	void OnCollisionStay2D(Collision2D coll) {

		if(coll.gameObject.tag == "Ground"){

			if ((Input.GetKey (KeyCode.LeftArrow)) || (Input.GetKey (KeyCode.A))){;
				GetComponent<Rigidbody2D>().AddTorque (1000.0f);
				GetComponent<Rigidbody2D>().AddForce(new Vector2(-400.0f, 0.0f));
			}
			
			if ((Input.GetKey (KeyCode.RightArrow)) || (Input.GetKey (KeyCode.D))){;
				GetComponent<Rigidbody2D>().AddTorque (-1000.0f);
				GetComponent<Rigidbody2D>().AddForce(new Vector2(400.0f, 0.0f));
			}

			if ((Input.GetKey (KeyCode.UpArrow)) || (Input.GetKey(KeyCode.W))){;
				if(!jumping){
					GetComponent<Rigidbody2D>().AddForce(new Vector2(0.0f, 6000.0f));
					jumping = true;
				}
			}
		}
	}
}
