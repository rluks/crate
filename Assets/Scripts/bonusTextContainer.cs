﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class bonusTextContainer : MonoBehaviour {

	GameObject target;

	// Use this for initialization
	void Start () {
		target = GameObject.Find ("crateHero");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void bonusCollected(){
		transform.parent = null;
		GetComponent<FixRotation> ().enabled = false;
		InvokeRepeating ("moveTowardsTarget", 0.0f, 0.01f);
	}

	void moveTowardsTarget(){
		transform.position = Vector3.MoveTowards(transform.position, target.transform.position, 0.1f);

		if (transform.position == target.transform.position){
			Invoke ("destroyMe", 10.0f);
		}
	}

	void destroyMe(){
		Destroy (this.gameObject);
	}
}
