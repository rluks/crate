﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class characterSlot : MonoBehaviour {

	public GameObject slot = null;
	public string requires;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool addToSlot(GameObject o){

		if (o.name == requires && slot == null) {
			//Debug.Log (o.name + " adding to " + this.name);
			o.transform.parent = transform;
			slot = o;
			GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f); 


			return true;
		}else{
			//Debug.Log (o.name + " NOT adding to " + this.name);
			return false;
		}

	}
}
