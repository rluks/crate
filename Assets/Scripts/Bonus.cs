﻿using UnityEngine;
using System.Collections;

public class Bonus : MonoBehaviour {

	int increaseCounter = 0;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("increaseFallingSpeed", 0.01f, 0.02f);
	}
	
	// Update is called once per frame
	void Update () {

	}

	void increaseFallingSpeed(){
		GetComponent<Rigidbody2D>().gravityScale += 0.02f;
		increaseCounter++;
		//Debug.Log("inc");
		//if(increaseCounter > 1
		if(GetComponent<Rigidbody2D>().gravityScale > 4.0f){
			//Debug.Log("stop");
			CancelInvoke("increaseFallingSpeed");
			GetComponent<Rigidbody2D>().gravityScale = 1.0f;
		}

	}
}
