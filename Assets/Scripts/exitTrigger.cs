﻿using UnityEngine;
using System.Collections;

public class exitTrigger : MonoBehaviour {

	void OnTriggerStay2D(Collider2D other){
		if(other.gameObject.name == "crateHero" && transform.parent.GetComponent<Tram>().isOpen){
			transform.parent.GetComponent<Tram>().exitActivated();
		}
	}
}
