﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gameMenu : MonoBehaviour {

	public bool menuEnabled = false;

	GameObject continueButton;

	// Use this for initialization
	void Start () {
		continueButton = GameObject.Find("continueButton");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setMenuElements(bool value){
		Image[] images = GetComponentsInChildren<Image> ();
		foreach (Image image in images) {
			image.enabled = value;
		}

		Button[] buttons = GetComponentsInChildren<Button> ();
		foreach (Button button in buttons) {
			button.enabled = value;
		}

		Text[] texts = GetComponentsInChildren<Text> ();
		foreach (Text text in texts) {
			text.enabled = value;
		}

		GameObject[] cubes = GameObject.FindGameObjectsWithTag ("brnoTextCube");
		foreach (GameObject cube in cubes) {
			cube.GetComponent<Renderer>().enabled = value;
		}

		GameObject[] cubesC = GameObject.FindGameObjectsWithTag ("brnoTextCubeContinue");
		foreach (GameObject cube in cubesC) {
			cube.GetComponent<Renderer>().enabled = value;
		}

		menuEnabled = value;
	}

	public void setContinueButton(bool value){
		continueButton.GetComponent<Image> ().enabled = value;
		continueButton.GetComponent<Button> ().enabled = value;
		continueButton.transform.GetChild (0).GetComponent<Text> ().enabled = value;

		GameObject[] cubes = GameObject.FindGameObjectsWithTag ("brnoTextCubeContinue");
		foreach (GameObject cube in cubes) {
			cube.GetComponent<Renderer>().enabled = value;
		}
	}
}
