﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour {

	public static GameplayManager instance { get; private set; }

	public static float TIME_BONUS = 3.0f;

	public float timeLeft;

	GameObject[] slots;
	GameObject exitIcon;
	GameObject notificationText;

	GameObject hero;
	GameObject tram;
	
	GameObject gameMenu;

	void Awake(){
		instance = this;
	}

	// Use this for initialization
	void Start () {
		Time.timeScale = 1.0f;

		timeLeft = 125.0f;
		InvokeRepeating ("countDown", 0.0f, 1.0f);

		slots = GameObject.FindGameObjectsWithTag ("Slot");

		exitIcon = GameObject.Find ("exitIcon");
		notificationText = GameObject.Find ("notificationText");

		gameMenu = GameObject.Find ("gameMenu");
		gameMenu.GetComponent<gameMenu> ().setMenuElements (false);

		hero = GameObject.Find ("crateHero");
		tram = GameObject.Find ("tram");

		setControls (true);

	}

	void countDown(){
		timeLeft -= 1.0f;

		if (timeLeft <= 0.0f) {
			CancelInvoke("countDown");
			timeLeft = 0.0f;
			timesUp();
		}
	}

	void timesUp(){
		Debug.Log ("cas vyprsel!");
		notificationText.GetComponent<notificationText> ().setText ("Time's up!");
		Time.timeScale = 0.0f;
		gameMenu.GetComponent<gameMenu> ().setMenuElements (true);
		gameMenu.GetComponent<gameMenu> ().setContinueButton (false);
		setControls (false);
	}

	void setControls(bool value){
		hero.GetComponent<Controls> ().enabled = value;
	}

	public void restart(){
		Application.LoadLevel (Application.loadedLevelName);
	}

	public void continueGame(){
		gameMenu.GetComponent<gameMenu> ().setMenuElements (false);
		Time.timeScale = 1.0f;
	}

	public void pause(){

		if (gameMenu.GetComponent<gameMenu> ().menuEnabled) {
			continueGame();
		}else {
			gameMenu.GetComponent<gameMenu> ().setMenuElements (true);
			Time.timeScale = 0.0f;
		}
	}

	public void quit(){
		Application.LoadLevel ("credits");
	}

	public void winStart(){
		setControls (false);
	}

	public void winFinished(){

		if(Application.loadedLevelName == "level0")
			Application.LoadLevel ("level1");

		if(Application.loadedLevelName == "level1")
			Application.LoadLevel ("credits");
	}

	public void bonus(GameObject b){

		timeLeft += TIME_BONUS;

		b.GetComponentInChildren<timeBonusText> ().setBonusText ();

		b.GetComponentInChildren<bonusTextContainer> ().bonusCollected ();

		Destroy(b.gameObject);
	}

	public void findSlot(GameObject o){
		foreach (GameObject slot in slots) {
			if(slot.GetComponent<characterSlot>().addToSlot(o)){
				Debug.Log("added " + o.name + " to " + slot.name);
				break;
			}
		}

		if (slotsFilled ()) {
			Debug.Log("all characters collected");
			notificationText.GetComponent<notificationText> ().setText ("Tram has arrived!");
			openExit();
		}
	}

	void openExit(){
		exitIcon.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f); 
		tram.GetComponent<Tram>().enableTram ();
	}

	bool slotsFilled(){
		int fillCount = 0;
		foreach (GameObject slot in slots) {
			if(slot.GetComponent<characterSlot>().slot != null){
				fillCount++;
			}
		}

		if (fillCount == 5) {
			return true;
		}else{
			return false;
		}
	}
}
