﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	ParticleSystem[] pss;

	void Awake(){
		pss = GetComponentsInChildren<ParticleSystem> ();
	}

	public void detonate(){
		foreach(ParticleSystem ps in pss){
			ps.Play();
		}
	}
}
