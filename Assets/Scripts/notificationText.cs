﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class notificationText : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setText(string s){
		GetComponent<Text> ().text = s;
		Invoke ("removeText", 3.0f);
	}

	void removeText(){
		GetComponent<Text> ().text = "";
	}
}
