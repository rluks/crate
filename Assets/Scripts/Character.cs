﻿using UnityEngine;
using System.Collections;


public class Character : MonoBehaviour {

	GameObject target;
	bool isInSlot = false;

	// Use this for initialization
	void Start () {
		target = GameObject.Find ("crateHero");
	}
	
	// Update is called once per frame
	void Update () {
		if(!isInSlot){
			if (transform.position == target.transform.position) {
				isInSlot = true;
				inCrate();
			}
		}
	}

	public void setColliders(bool value){
		Collider2D[] colls = GetComponentsInChildren<Collider2D> ();
		foreach (Collider2D coll in colls) {
			coll.enabled = value;
		}
	}

	public void vanish(){
		InvokeRepeating ("vanishRepeat", 0.0f, 0.01f);
	}

	void vanishRepeat(){
		transform.localScale *= 0.99f;
		//transform.position = new Vector3 (transform.position.x, transform.position.y, transform.position.z-1);
		//transform.localScale = transform.localScale * 1.01f;
		transform.position = Vector3.MoveTowards(transform.position, target.transform.position, 0.1f);
		if (transform.localScale.magnitude < 0.1f)
			transform.position = target.transform.position;
	}

	void inCrate(){
		transform.parent = target.transform;
		GameplayManager.instance.findSlot (this.gameObject);
	}

}
