﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tram : MonoBehaviour {

	public bool isOpen = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void enableTram(){
		GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f); 
		isOpen = true;
	}

	public void exitActivated(){
		GameplayManager.instance.winStart();
		InvokeRepeating("scale", 0.0f, 0.0025f);
	}

	void scale(){
		transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, 0.0f), 0.1f);
		transform.localScale *= 1.00025f;
		if (transform.localScale.magnitude > 30.0f) {
			CancelInvoke("scale");
			GameplayManager.instance.winFinished();
		}
	}
	
}
